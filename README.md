# The algorithm-utilities package

The [ARISE project](https://www.arise-biodiversity.nl/) is building an infrastructure that will identify and monitor all
multicellular species in the Netherlands. Within ARISE, the [digital species identification (DSI) team]
(https://www.arise-biodiversity.nl/teamdigitalspeciesidentification) is building services that support the development
and deployment of AI algorithms which detect and identify species from a wide variety of digital media such as sound,
images, and radar.

To make it as easy as possible to connect AI algorithms to the ARISE DSI system, an [algorithm template]
(https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template) has been created. The template offers common
integration functionality that can be reused by all algorithms. The output of the algorithms can successfully be
processed by the ARISE DSI system if the analysis results format is used. See the
[ARISE DSI algorithm development guidelines]
(https://docs.google.com/document/d/1AjJSUXoVktqeaqf0_7ZK8ka4pM1t-dPtdbDBRXL5-qs/edit) for more information and
examples. The [ARISE machine prediction API]
(https://docs.google.com/document/d/1TQJtAJaEib5KZSNE6RFeg31QH1X_efiKw0KiB9RLvGI/edit) contains additional technical
documentation. Please contact the ARISE DSI team if you have any questions.

# Use the algorithm-utilities package
This algorithm-utilities repository contains Python classes that are both used by the algorithm template and the ARISE
DSI system. When you use the algorithm template, the classes from this package will be automatically available. If for
some reason you prefer not to use the algorithm template, you can also add this package to your project as described in
the next paragraph. You can find more information on integrating your algorithm with ARISE in the [algorithm template]
(https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template) repository.

GitLab supports Python packages with the [package registry](https://docs.gitlab.com/ee/user/packages/pypi_repository/).
The GitLab API provides a URL to make a package available, which can be added to your requirements.txt file. The
algorithm-utilities project ID is 53850130. To add the package to your project, you can add these two lines to your
requirements.txt file:

```requirements
git+https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-utilities.git@v1.1.0
```

Or, if you version of pip does not support [PEP 517](https://pypi.org/project/pep517/), add the following:

```requirements
--index-url https://gitlab.com/api/v4/projects/53850130/packages/pypi/simple
algorithm-utilities==1.0.0
```

# Use the algorithm-utilities classes
See the [algorithm template](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-template) for an example AI
algorithm that is integrated with the ARISE DSI system and makes use of the algorithm-utilities package.

A short example that uses some of the algorithm-utilities classes:

```Python
from algorithm_utilities.arise.dsi.algorithm.utilities.analysis_data_classes import ImageBoundingBox, Prediction

print("Testing the algorithm-utilities package.")

prediction = Prediction(
    name="Ara ararauna",
    probability=0.98,
    segment=ImageBoundingBox(x1=0.20, y1=0.18, x2=0.66, y2=0.89),
    # segment=SoundFragment(t1=0.66, t2=0.89),
)

print(f"Prediction: {prediction}.")

```

# Build the package
Update the package version number by updating the `pyproject.toml` file or using poetry (and updating this README file):

```bash
poetry version patch
```

To view the options for setting the version number, check [poetry version](https://python-poetry.org/docs/cli/#version).

Create a source distribution and a wheel:

```bash
poetry build
```

Add the Twine package if you do not have it already and check if the build went well:

```bash
poetry add twine
poetry run twine check dist/*
```

# Publish the package
> It is not possible to publish a package if a package of the same _name_ and _version_ already exists. Make sure to
> change the version (or that the package does not exist yet in the GitLab registry).

Upload the package to GitLab with Twine, using inline authentication. The token credentials can be found in BitWarden.

```bash
TWINE_PASSWORD=<token_password> TWINE_USERNAME=<token_username> python3 -m twine upload --repository-url https://gitlab.com/api/v4/projects/53850130/packages/pypi dist/*
```

Check whether the new package was uploaded successfully: [GitLab registry](https://gitlab.com/arise-biodiversity/DSI/algorithms/algorithm-utilities/-/packages).

# Support
Please let us know if you have questions, suggestions for improvements or other feedback. You can email Jacob Kamminga
(j.w.kamminga@utwente.nl), leader of the ARISE DSI team.

# License
This algorithm-utilities package is licensed under the Apache License 2.0.
