import json
import pathlib
import unittest

from typing import Any

from algorithm_utilities.arise.dsi.algorithm.utilities.analysis_results_util import (
    AnalysisResultValidator,
    AnalysisResultParser,
    AnalysisResultFilter,
    AnalysisResultBuilder,
    AnalysisResultException,
)

dummy_data_folder = f"{pathlib.Path(__file__).parent.resolve()}/dummy_data"


class TestAnalysisResultParser(unittest.TestCase):
    default_fields: dict[str, Any] = {
        "predictions": [],
        "media": [],
        "region_groups": [],
        "generated_by": {},
    }

    def setUp(self):
        self.parser = AnalysisResultParser()

    def test_correct_data(self):
        dummy_data_path = f"{dummy_data_folder}/correct_data.json"

        with open(dummy_data_path) as dummy_data:
            self.parser.parse(json.load(dummy_data))

    # Top level tests
    def test_top_level_fields_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as region_group_error:
            self.parser.parse({"predictions": [], "media": [], "generated_by": {}})
        with self.assertRaises(AnalysisResultException) as prediction_error:
            self.parser.parse({"region_groups": [], "media": [], "generated_by": {}})
        with self.assertRaises(AnalysisResultException) as media_error:
            self.parser.parse({"region_groups": [], "predictions": [], "generated_by": {}})
        with self.assertRaises(AnalysisResultException) as generated_by_error:
            self.parser.parse({"region_groups": [], "media": [], "predictions": []})

        self.assertEqual(
            region_group_error.exception.message,
            "'region_groups' is a required property",
        )
        
        self.assertEqual(prediction_error.exception.message, "'predictions' is a required property")

        self.assertEqual(media_error.exception.message, "'media' is a required property")
        self.assertEqual(
            generated_by_error.exception.message,
            "'generated_by' is a required property",
        )

    def test_top_level_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as region_group_error:
            self.parser.parse({**self.default_fields, "region_groups": {}})
        with self.assertRaises(AnalysisResultException) as prediction_error:
            self.parser.parse({**self.default_fields, "predictions": {}})
        with self.assertRaises(AnalysisResultException) as media_error:
            self.parser.parse({**self.default_fields, "media": {}})
        with self.assertRaises(AnalysisResultException) as generated_by_error:
            self.parser.parse({**self.default_fields, "generated_by": []})

        for exception in [region_group_error, prediction_error, media_error]:
            self.assertEqual(exception.exception.message, "{} is not of type 'array'")

        self.assertEqual(generated_by_error.exception.message, "[] is not of type 'object'")
        

    def test_top_level_single_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as region_group_error:
            self.parser.parse({**self.default_fields, "region_groups": [[]]})
        with self.assertRaises(AnalysisResultException) as prediction_error:
            self.parser.parse({**self.default_fields, "predictions": [[]]})
        with self.assertRaises(AnalysisResultException) as media_error:
            self.parser.parse({**self.default_fields, "media": [[]]})

        for exception in [region_group_error, prediction_error, media_error]:
            self.assertEqual(exception.exception.message, "[] is not of type 'object'")

    # `media` tests
    def test_single_media_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse({**self.default_fields, "media": [{}]})
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse({**self.default_fields, "media": [{"id": ""}]})

        self.parser.parse({**self.default_fields, "media": [{"id": "", "filename": ""}]})

        self.assertEqual(exception1.exception.message, "'id' is a required property")
        self.assertEqual(exception2.exception.message, "'filename' is a required property")
        
    def test_single_media_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse({**self.default_fields, "media": [{"id": 0, "filename": ""}]})
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse({**self.default_fields, "media": [{"id": "", "filename": 0}]})

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "0 is not of type 'string'")

    # `generated_by` tests
    def test_generated_by_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "generated_by": {"datetime": 0, "version": "", "tag": ""},
                }
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "generated_by": {"datetime": "", "version": 0, "tag": ""},
                }
            )
        with self.assertRaises(AnalysisResultException) as exception3:
            self.parser.parse(
                {
                    **self.default_fields,
                    "generated_by": {"datetime": "", "version": "", "tag": 0},
                }
            )
        self.parser.parse(
            {
                **self.default_fields,
                "generated_by": {"datetime": "", "version": "", "tag": ""},
            }
        )

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception3.exception.message, "0 is not of type 'string'")

    # 'region_groups' tests
    def test_single_region_group_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse({**self.default_fields, "region_groups": [{}]})
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse({**self.default_fields, "region_groups": [{"id": ""}]})

        self.assertEqual(exception1.exception.message, "'id' is a required property")
        self.assertEqual(exception2.exception.message, "'regions' is a required property")

    def test_single_region_group_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse({**self.default_fields, "region_groups": [{"id": 0, "regions": []}]})
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse({**self.default_fields, "region_groups": [{"id": "", "regions": {}}]})
        with self.assertRaises(AnalysisResultException) as exception3:
            self.parser.parse(
                {
                    **self.default_fields,
                    "region_groups": [{"id": "", "regions": [], "individual_id": 0}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception4:
            self.parser.parse(
                {
                    **self.default_fields,
                    "region_groups": [{"id": "", "regions": [[]], "individual_id": ""}],
                },
            )

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "{} is not of type 'array'")
        self.assertEqual(exception3.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception4.exception.message, "[] is not of type 'object'")

    def test_single_region_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as exception:
            self.parser.parse(
                {
                    **self.default_fields,
                    "region_groups": [{"id": "", "individual_id": "", "regions": [{}]}],
                },
            )

        self.assertEqual(exception.exception.message, "'media_id' is a required property")


    def test_single_region_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "region_groups": [{"id": "", "individual_id": "", "regions": [{"media_id": 0}]}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "region_groups": [
                        {
                            "id": "",
                            "individual_id": "",
                            "regions": [{"media_id": "", "box": []}],
                        }
                    ],
                },
            )

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "[] is not of type 'object'")

    def test_single_box_wrong_type(self):
        properties = ["x1", "x2", "y1", "y2", "z1", "z2", "t1", "t2"]

        for property_name in properties:
            data = {
                **self.default_fields,
                "region_groups": [
                    {
                        "id": "",
                        "individual_id": "",
                        "regions": [{"media_id": "", "box": {property_name: ""}}],
                    }
                ],
            }

            with self.assertRaises(AnalysisResultException) as exception:
                self.parser.parse(data)

            self.assertEqual(exception.exception.message, "'' is not of type 'number'")

    # 'predictions' tests
    def test_single_prediction_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse({**self.default_fields, "predictions": [{}]})

        self.assertEqual(exception1.exception.message, "'region_group_id' is a required property")

    def test_single_prediction_wrong_type(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": 0, "taxa": {"type": "region", "items": []}}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": []}],
                },
            )

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "[] is not of type 'object'")

    def test_taxa_missing_fields(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": {}}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": {"type": ""}}],
                },
            )

        self.assertEqual(exception1.exception.message, "'type' is a required property")

        self.assertEqual(exception2.exception.message, "'items' is a required property")

    def test_taxa_wrong_types(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": {"type": 0, "items": []}}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": {"type": "", "items": {}}}],
                },
            )

        self.assertEqual(exception1.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception2.exception.message, "{} is not of type 'array'")


    def test_taxa_item_wrong_types(self):
        with self.assertRaises(AnalysisResultException) as exception1:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [{"region_group_id": "", "taxa": {"type": "", "items": [[]]}}],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception2:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [
                        {
                            "region_group_id": "",
                            "taxa": {
                                "type": "",
                                "items": [{"probability": "", "scientific_name": ""}],
                            },
                        }
                    ],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception3:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [
                        {
                            "region_group_id": "",
                            "taxa": {
                                "type": "",
                                "items": [{"probability": 0, "scientific_name": 0}],
                            },
                        }
                    ],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception4:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [
                        {
                            "region_group_id": "",
                            "taxa": {
                                "type": "",
                                "items": [{"probability": 2, "scientific_name": ""}],
                            },
                        }
                    ],
                },
            )
        with self.assertRaises(AnalysisResultException) as exception5:
            self.parser.parse(
                {
                    **self.default_fields,
                    "predictions": [
                        {
                            "region_group_id": "",
                            "taxa": {
                                "type": "",
                                "items": [{"probability": -1, "scientific_name": ""}],
                            },
                        }
                    ],
                },
            )

        self.assertEqual(exception1.exception.message, "[] is not of type 'object'")
        self.assertEqual(exception2.exception.message, "'' is not of type 'number'")
        self.assertEqual(exception3.exception.message, "0 is not of type 'string'")
        self.assertEqual(exception4.exception.message, "2 is greater than the maximum of 1")
        self.assertEqual(exception5.exception.message, "-1 is less than the minimum of 0")

    # Validation tests
    def test_validate_referential_integrity_missing_region_group_ids(self):
        dummy_data_path = f"{dummy_data_folder}/missing_region_group_ids.json"

        with self.assertRaises(AnalysisResultException) as exception:
            with open(dummy_data_path) as dummy_data:
                self.parser.parse(json.load(dummy_data))

        self.assertEqual(
            exception.exception.message,
            "Error: Found 1 predictions[]->region_group_id without a corresponding region_groups[]->id.",
        )

    def test_validate_dimensions_missing_x2(self):
        dummy_data_path = f"{dummy_data_folder}/missing_x2.json"

        with self.assertRaises(AnalysisResultException) as exception:
            with open(dummy_data_path) as dummy_data:
                self.parser.parse(json.load(dummy_data))

        self.assertEqual(exception.exception.message, "Error: Incomplete spatial dimensions in region box.")

    def test_validate_dimensions_x1_greater_than_x2(self):
        dummy_data_path = f"{dummy_data_folder}/x1_greater_than_x2.json"

        with self.assertRaises(AnalysisResultException) as exception:
            with open(dummy_data_path) as dummy_data:
                self.parser.parse(json.load(dummy_data))

        self.assertEqual(exception.exception.message, "Error: Incomplete spatial dimensions in region box.")

    def test_validate_dimensions_missing_all(self):
        dummy_data_path = f"{dummy_data_folder}/missing_all.json"

        with self.assertRaises(AnalysisResultException) as exception:
            with open(dummy_data_path) as dummy_data:
                self.parser.parse(json.load(dummy_data))

        self.assertEqual(
            exception.exception.message,
            "Error: region has a box but it contains neither spatial '(x1, y1)' nor temporal '(t1)' extents.",
        )

    def test_validate_dimensions_missing_y1(self):
        dummy_data_path = f"{dummy_data_folder}/missing_y1.json"

        with self.assertRaises(AnalysisResultException) as exception:
            with open(dummy_data_path) as dummy_data:
                self.parser.parse(json.load(dummy_data))

        self.assertEqual(exception.exception.message, "Error: Incomplete spatial dimensions in region box.")
