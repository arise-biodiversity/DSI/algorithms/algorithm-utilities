#!/usr/bin/env bash
function run {
    pip install --upgrade poetry
    poetry lock
    poetry install
    poetry run pytest tests/
}

function in_docker {
    docker run -v $PWD:/src:z -w /src --rm -it python:3.11 ./scripts/test.sh "$@";
}

if [ -z "$@" ]; then
    in_docker run
else
    "$@";
fi
