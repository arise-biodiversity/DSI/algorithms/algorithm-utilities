from enum import Enum


# The classes with the "Json" prefix map one-on-one to the analysis results JSON.


class JsonTaxaItem:
    def __init__(self, taxa_item_id: str, name: str, probability: float):
        self.taxa_item_id = taxa_item_id
        self.name = name
        self.probability = probability


class JsonTaxa:
    def __init__(self, taxa_type: str, taxa_items: list[JsonTaxaItem]):
        self.taxa_type = taxa_type
        self.taxa_items = taxa_items


class JsonPrediction:
    def __init__(self, region_group_id: str, taxa: JsonTaxa):
        self.region_group_id = region_group_id
        self.taxa = taxa


class JsonRegionBox:
    def __init__(self, **kwargs: float):
        self.coordinates = kwargs
        self._validate()

    def _validate(self):
        for coordinate, value in self.coordinates.items():
            if coordinate not in ["x1", "y1", "x2", "y2", "t1", "t2"]:
                raise ValueError(f"Unexpected coordinate name '{coordinate}' in region box '{self.coordinates}'.")

            if not isinstance(value, float):
                raise ValueError(f"Unexpected coordinate value '{value}' in region box '{self.coordinates}'.")


class JsonRegion:
    def __init__(self, media_id: str, box: JsonRegionBox):
        self.media_id = media_id
        self.box = box


class JsonRegionGroup:
    def __init__(self, region_group_id: str, individual_id: str, regions: list[JsonRegion]):
        self.region_group_id = region_group_id
        self.individual_id = individual_id
        self.regions = regions


class JsonMedia:
    def __init__(self, media_item_id: str, filename: str):
        self.media_item_id = media_item_id
        self.filename = filename


# todo: Add name [schema] and generated_by (JsonGeneratedBy: datetime, version
#       [algorithm_version] & tag [algorithm_prefix/name]) fields to this class.
class JsonAlgorithmAnalysisResults:
    def __init__(
        self,
        json_media_list: list[JsonMedia],
        json_region_groups: list[JsonRegionGroup],
        json_predictions: list[JsonPrediction],
    ):
        self.json_media_list = json_media_list
        self.json_region_groups = json_region_groups
        self.json_predictions = json_predictions

    # We could return a dict from media item ID to list[Prediction].
    @property
    def predictions(self) -> list["Prediction"]:
        prediction_list = []

        for region_group in self.json_region_groups:
            for json_prediction in self.json_predictions:
                if json_prediction.region_group_id == region_group.region_group_id:
                    for json_taxa_item in json_prediction.taxa.taxa_items:
                        for region in region_group.regions:
                            prediction_list.append(
                                Prediction(
                                    json_taxa_item.name,
                                    100 * json_taxa_item.probability,
                                    region.box,
                                )
                            )

        return prediction_list


class AlgorithmMode(Enum):
    Directories = 1
    Endpoint = 2


class MediaDataItem:
    def __init__(self, media_id: str, file_path: str, filename: str):
        self.media_id = media_id
        self.file_path = file_path
        self.filename = filename
        self.predictions = None


class ImageBoundingBox(JsonRegionBox):
    def __init__(self, x1: float, y1: float, x2: float, y2: float):
        super().__init__(x1=x1, y1=y1, x2=x2, y2=y2)
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2


class SoundFragment(JsonRegionBox):
    def __init__(self, t1: float, t2: float):
        super().__init__(t1=t1, t2=t2)
        self.t1 = t1
        self.t2 = t2


class Prediction:
    def __init__(self, name: str, probability: float, segment: JsonRegionBox):
        self.name = name
        self.probability = probability
        self.segment = segment
