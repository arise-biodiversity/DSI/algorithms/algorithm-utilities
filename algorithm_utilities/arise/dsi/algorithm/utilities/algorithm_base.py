import abc
import flask
import json
import logging
import os
import sys
import tempfile

# from flask import request
from flask.wrappers import Response
from multiprocessing import Process
from typing import Any, Dict, List, Optional

from algorithm_utilities.arise.dsi.algorithm.utilities.analysis_results_util import AnalysisResultBuilder
from algorithm_utilities.arise.dsi.algorithm.utilities.analysis_data_classes import AlgorithmMode, MediaDataItem


class AlgorithmBase(abc.ABC):
    """
    Abstract base class for AI algorithms with built-in functionality to integrate with
    the ARISE DSI workflow. This functionality consists of two different modes of
    communication between the algorithm and the framework (using directories or an
    endpoint) and support for converting the analysis results to the format that the
    ARISE DSI system can handle (using the AnalysisResultsBuilder class).

    Derived classes only need to implement the analyse_media_items method (which is
    abstract in this class). Look at the

    Environment variables
    =====================
    There are several environment variables that can provide data to the algorithm and
    can influence the way the algorithm runs:
    - ALGORITHM_NAME_PREFIX: The algorithm name prefix, default: "unknown-algorithm".
                             The prefix is defined as the part of the name until the
                             first space in lowercase.
    - ALGORITHM_VERSION: The version of the algorithm, default: "unknown-version".

    - ALGORITHM_MODE: "Directories" or "Endpoint", default: "Directories".
    * If the algorithm mode is "Directories":
      + INPUT_DIRECTORY: The directory with input files, default: "algorithm-input".
      + OUTPUT_DIRECTORY: The directory for output files, default: "algorithm-output".
      + ANALYSIS_RESULTS_FILENAME: The filename for the analysis results, default:
                                   "analysis-results.json".
    * If the algorithm mode is "Endpoint":
      + ENDPOINT_HOST: The endpoint host to listen for requests, default: "0.0.0.0"
                       (see https://flask.palletsprojects.com/en/3.0.x/quickstart).
      + ENDPOINT_PORT: The port number to listen for requests, default: "5000".
      + ENDPOINT_POST_PATH: The post path to listen for requests, default: "v1/analyse".
      + ENDPOINT_POST_VARIABLE_NAME: The post parameter name, default: "media".

    - LOG_DIRECTORY: The directory where the log file will be created, default: the
                     current working directory.
    - LOG_FILE_NAME: The base file name of the log file, default: the algorithm prefix.
                     The log file extension is ".log".
    """

    def __init__(
        self,
        default_algorithm_prefix: str = "unknown-algorithm",
        default_algorithm_version: str = "unknown-version",
        default_algorithm_mode: str = AlgorithmMode.Directories.name,
        default_input_directory: str = "algorithm-input",
        default_output_directory: str = "algorithm-output",
        default_analysis_results_filename: str = "analysis-results.json",
        default_endpoint_port: str = "5000",
    ):
        self._algorithm_prefix = os.getenv("ALGORITHM_NAME_PREFIX", default_algorithm_prefix)
        self._algorithm_version = os.getenv("ALGORITHM_VERSION", default_algorithm_version)

        self._logger = self._get_logger(self._algorithm_prefix)
        self._logger.info("======")

        self._algorithm_mode = self._determine_algorithm_mode(default_algorithm_mode)

        if self._algorithm_mode == AlgorithmMode.Directories:
            self._input_directory = os.getenv("INPUT_DIRECTORY", default_input_directory)
            self._output_directory = os.getenv("OUTPUT_DIRECTORY", default_output_directory)
            self._analysis_results_filename = os.getenv("ANALYSIS_RESULTS_FILENAME", default_analysis_results_filename)

            os.makedirs(self._input_directory, exist_ok=True)
            os.makedirs(self._output_directory, exist_ok=True)
        elif self._algorithm_mode == AlgorithmMode.Endpoint:
            self._endpoint_port = os.getenv("ENDPOINT_PORT", default_endpoint_port)

        # self._flask_is_running: bool = False
        self._flask_process: Optional[Process] = None

    @property
    def input_directory(self) -> str:
        return self._input_directory

    @property
    def output_directory(self) -> str:
        return self._output_directory

    @property
    def analysis_results_filename(self) -> str:
        return self._analysis_results_filename

    @abc.abstractmethod
    def analyse_media_items(self, media_data_items: List[MediaDataItem]) -> None:
        """
        Perform analysis on a batch of media files. This method needs to be implemented
        by each algorithm.

        :param media_data_items: A list of media data items.
        Returns: None, the algorithm predictions are added to the media data items.
        """
        ...

    def run(self):
        if self._algorithm_mode:
            self._logger.info(f"Running '{self._algorithm_prefix}' {self._algorithm_version} in {self._algorithm_mode.name.lower()} mode.")

            if self._algorithm_mode == AlgorithmMode.Directories:
                # Process all the media files in the input directory and exit.
                self.analyse_directories()
            elif self._algorithm_mode == AlgorithmMode.Endpoint:
                # Start the web service and wait for the endpoint to be called. The
                # algorithm will keep running until the process is terminated.
                self.initialise_endpoint()

    def analyse_directories(self) -> None:
        # Get the media file paths.
        self._logger.info(f"Reading media files from the '{self._input_directory}' directory.")

        input_paths = [
            os.path.join(self._input_directory, file_name)
            for file_name in os.listdir(self._input_directory)
            if os.path.isfile(os.path.join(self._input_directory, file_name))
        ]

        # Analyse the media files.
        analysis_results = self.analyse(input_paths)

        # Write the analysis results to a JSON file.
        results_path = os.path.join(self._output_directory, "analysis-results.json")
        message = f"Write the analysis results to the JSON file '{results_path}'."
        self._logger.info(message)
        with open(results_path, "w", encoding="utf-8") as results_file:
            json.dump(analysis_results, results_file, ensure_ascii=False, indent=4)  # type: ignore

    def initialise_endpoint(self) -> None:
        host = os.getenv("ENDPOINT_HOST", "0.0.0.0")
        port = int(os.getenv("ENDPOINT_PORT", self._endpoint_port))

        flask_app = flask.Flask(__name__)
        flask_app.logger.setLevel(logging.DEBUG)

        rule_post_path = os.getenv("ENDPOINT_POST_PATH", "/v1/analyse")
        flask_app.add_url_rule(
            rule=rule_post_path,
            endpoint=self.analyse_endpoint.__name__,
            view_func=self.analyse_endpoint,
            methods=["POST"],
        )

        # @flask_app.route("/", methods=["GET", "POST"])
        # @flask_app.route("/<first>", methods=["GET", "POST"])
        # @flask_app.route("/<first>/<path:rest>", methods=["GET", "POST"])
        # def fallback(first=None, rest=None):
        #     self._logger.warning(
        #         f"Warning: unexpected Flask request: {flask.request}, first part of "
        #         f"URL: {first}, rest of URL: {rest}.")

        url_rules = ["/", "/<first>", "/<first><path:rest>"]
        for url_rule in url_rules:
            flask_app.add_url_rule(
                rule=url_rule,
                endpoint=self.fallback.__name__,
                view_func=self.fallback,
                methods=["GET", "POST"],
            )

        self._logger.info(f"Start Flask app on host '{host}' and port '{port}'.")
        # The debugger allows executing arbitrary Python code, so it should be switched
        # off.
        # flask_app.run(host, port, debug=False)
        self._flask_process = Process(target=flask_app.run, args=(host, port, True))
        self._flask_process.start()

        # self._flask_is_running = True

    def analyse_endpoint(self) -> Response:
        self._logger.info(f"Received Flask request: {flask.request}.")

        # Get the uploaded media files.
        post_variable_name = os.getenv("ENDPOINT_POST_VARIABLE_NAME", "media")
        uploaded_files = flask.request.files.getlist(post_variable_name)

        # Write the uploaded files to a temporary directory. Upon exiting the "with"
        # context, the directory (and its contents) are removed.
        with tempfile.TemporaryDirectory() as temporary_directory:
            media_file_paths = []

            for uploaded_file in uploaded_files:
                uploaded_name = uploaded_file.filename
                if uploaded_name:
                    file_path = os.path.join(temporary_directory, uploaded_name)
                    media_file_paths.append(file_path)
                    with open(file_path, "wb") as media_file:
                        media_file.write(uploaded_file.read())

            analysis_results = self.analyse(media_file_paths)

        return flask.jsonify(analysis_results)

    def fallback(self, first=None, rest=None) -> Response:
        # self._logger.warning(
        print(f"Warning: unexpected Flask request: {flask.request}, first part of URL: {first}, rest of URL: {rest}.")

        return flask.jsonify({"warning": f"Unexpected Flask request: {flask.request}."})

    def analyse(self, media_file_paths: List[str]) -> Dict[str, Any]:
        """
        Analyse a batch of media files and return the results in ARISE DSI JSON format.
        """
        self._logger.info(f"Number of media files: {len(media_file_paths)}.")

        if not media_file_paths:
            self._logger.error("No media files were found.")

        media_data_items = [
            MediaDataItem(media_id, media_file_path, filename)
            for media_file_path in media_file_paths
            for filename in [os.path.split(media_file_path)[-1]]
            for media_id in [os.path.splitext(filename)[0]]
        ]

        # Perform analysis on a batch of media files.
        self._logger.debug(
            f"Analysing {len(media_data_items)} media file"
            f"{'s' if len(media_data_items) != 1 else ''} "
            f"{[media_data_item.filename for media_data_item in media_data_items]}."
        )
        self.analyse_media_items(media_data_items)

        # Remove the media files once they have been processed.
        for media_file_path in media_file_paths:
            self._logger.debug(f"Remove the processed media file '{media_file_path}'.")
            os.unlink(media_file_path)

        builder = AnalysisResultBuilder()
        return builder.create_analysis_results(self._algorithm_prefix, self._algorithm_version, media_data_items)

    def stop(self):
        if self._algorithm_mode == AlgorithmMode.Endpoint:  # and self._flask_is_running:
            # shutdown_function = request.environ.get("werkzeug.server.shutdown")
            #
            # if shutdown_function:
            #     shutdown_function()
            # else:
            #     raise RuntimeError(
            #         "Error shutting down the Flask app: not running with the "
            #         "Werkzeug Server."
            #     )

            if self._flask_process:
                self._flask_process.terminate()
                self._flask_process.join()

                self._flask_process = None

                # self._flask_is_running = False

    def _get_logger(self, logger_name: str) -> logging.Logger:
        """
        Configures a logger that writes logging to both a log file and the console. The
        environment variables LOG_FOLDER and LOG_FILE_NAME can be used to specify the
        file log path. The file extension is ".log".
        :param logger_name: the name of the logger to create.
        :return: the logger.
        """
        algorithm_logger = logging.getLogger(logger_name)
        algorithm_logger.setLevel(logging.DEBUG)
        algorithm_logger.handlers = []

        formatter = logging.Formatter("%(asctime)s [%(name)s] %(message)s")

        log_directory = os.environ.get("LOG_DIRECTORY", os.getcwd())
        os.makedirs(log_directory, exist_ok=True)
        log_filename = os.getenv("LOG_FILE_NAME", f"{logger_name}.log")
        log_filepath = os.path.join(log_directory, log_filename)

        file_logger = logging.FileHandler(log_filepath)
        file_logger.setFormatter(formatter)
        file_logger.setLevel(logging.DEBUG)
        algorithm_logger.addHandler(file_logger)

        console_logger = logging.StreamHandler(sys.stdout)
        console_logger.setFormatter(formatter)
        console_logger.setLevel(logging.DEBUG)
        algorithm_logger.addHandler(console_logger)

        return algorithm_logger

    def _determine_algorithm_mode(self, default_algorithm_mode: str) -> Optional[AlgorithmMode]:
        algorithm_mode_str = os.getenv("ALGORITHM_MODE", default_algorithm_mode)

        try:
            return AlgorithmMode[algorithm_mode_str]

        except IndexError:
            self._logger.exception(f"Unexpected algorithm mode: '{algorithm_mode_str}', terminating.")

            return None
